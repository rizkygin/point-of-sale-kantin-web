<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class outlet extends Model
{
    protected $table = 'outles';

    public function outlet_menus(){
        return $this->hasMany('App\outlet_menu');
    }
    public function location(){
        return $this->belongsTo('App\location','location_id');
    }
}
