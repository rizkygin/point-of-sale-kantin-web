<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class basket extends Model
{
    protected $table = "baskets";

    public function table(){
        return $this->belongsTo('App\table','table_id');
    }
    public function outlet_menu(){
        return $this->belongsTo('App\outlet_menu','outlet_menu_id');
    }
}
