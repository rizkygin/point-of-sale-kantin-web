<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class transection extends Model
{
    protected $table = 'transections';

    public function table(){
        return $this->belongsTo('App\table','table_id');
    }
    public function order(){
        return $this->belongsTo('App\order_detail','order_id');
    }
}
