<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class role_user extends Model
{
    protected $table =  'role_users';

    public function users(){
        return $this->hasMany('App\User');
    }
}
