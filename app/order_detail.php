<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class order_detail extends Model
{
    protected $table = 'order_details';

    public function transection(){
        return $this->hasOne('App\transection');
    }
    public function outlet_menu(){
        return $this->belongsTo('App\outlet_menu','outlet_menu_id');
    }
}
