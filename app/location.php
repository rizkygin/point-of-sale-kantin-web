<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class location extends Model
{
    protected $table = 'locations';

    public function outles(){
        return $this-> hasMany('App/outlet');
    }
    public function tables(){
        return $this-> hasMany('App/table');
    }
    public function users(){
        return $this-> belongsTo('App/User','user_id');
    }
    public function city(){
        return $this-> belongsTo('App/city','city_id');
    }
}
