<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class outlet_menu extends Model
{
    protected $table = 'outlet_menus';

    public function basket(){
        return $this->hasMany('App\Basket');
    }
    public function order_detail(){
        return $this->hasMany('App\order_detail');
    }
    public function menu(){
        return $this->belongsTo('App\menu','menu_id');
    }
    public function outlet(){
        return $this->belongsTo('App\outlet','outlet_id');
    }
}
