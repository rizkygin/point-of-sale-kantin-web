<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class table extends Model
{
     protected $table = 'tables';

     public function basket(){
        return $this->hasMany('App\basket');
     }
     public function transection(){
        return $this->hasMany('App\transection');
     }
     public function location(){
         return $this->belongsTo('App\location','location_id');
     }
}
